package sheridan;

import java.util.regex.Pattern;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		
		Pattern validLogin = Pattern.compile("[A-Za-z0-9]");
		
		boolean isValid = true;
		
		if(loginName.length() < 6) {
			isValid = false;
		} 
		if (!validLogin.matcher(loginName).find()) {
			isValid = false;
		}
		
		return isValid;
		
	}
}
