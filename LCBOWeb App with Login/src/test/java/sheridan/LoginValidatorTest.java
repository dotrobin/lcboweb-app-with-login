package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {
	
	// Testing Over 6
	
	@Test 
	public void testIsValidLoginOver6Regular() {
		assertTrue("Login Success", LoginValidator.isValidLoginName("robinlansiquot"));
	}
	
	@Test
	public void testIsValidLoginOver6Exception() {
		assertFalse("Login Failed", LoginValidator.isValidLoginName("ro"));
	}
	
	@Test
	public void testIsValidLoginOver6BoundaryIn() {
		assertTrue("Login Success", LoginValidator.isValidLoginName("robin1"));
	}
	
	@Test
	public void testIsValidLoginOver6BoundaryOut() {
		assertFalse("Login Failed", LoginValidator.isValidLoginName("robin"));
	}
	
	// Testing for characters and numbers
	
	@Test
	public void testIsValidLoginHasCharactersAndDigitsRegular() {
		assertTrue("Login Success", LoginValidator.isValidLoginName("robinlansiquot"));
	}
	
	@Test
	public void testIsValidLoginHasCharactersAndDigitsException() {
		assertFalse("Login Failed", LoginValidator.isValidLoginName("@@@@@@"));
	}
	
	@Test
	public void testIsValidLoginHasCharactersAndDigitsBoundaryIn() {
		assertTrue("Login Success", LoginValidator.isValidLoginName("robin1"));
	}
	
	@Test
	public void testIsValidLoginHasCharactersAndDigitsBoundaryOut() {
		assertFalse("Login Failed", LoginValidator.isValidLoginName("robin"));
	}
	

}
